﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace App_Contenidos_Web_Service.Dao

{
    public class Cls_Contenidos_Dao : iContenidos_Dao
    {
        public DataTable Obtener_Contenidos(string cursoUsuarioId, SqlConnection obj_Conexion)
        {
            SqlDataAdapter DataAdapter = new SqlDataAdapter();
            String MyQuery = "";
            DataTable dt = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter();
            using (SqlCommand Obj_Comando = obj_Conexion.CreateCommand())
            {
                MyQuery = " SELECT cnt.Contenido_ID, cnt.Nombre_Contenido,cnt.Descripcion,cnt.Total_Duracion,cnt.Total_Paginas,cnt.Url_Acceso,cnt.Estatus,cnt.Extension_Archivo " +
                    "FROM Curso_Usuario cu,Curso c, Curso_Contenidos cc, Contenidos cnt " +
                    "WHERE cu.Curso_Usuario_ID = @cuid" +
                    " AND c.Curso_ID = cu.Curso_ID " +
                    " AND cc.Curso_ID = c.Curso_ID" +
                    " AND cnt.Contenido_ID = cc.Contenido_ID";
                Obj_Comando.Parameters.Add("@cuid", SqlDbType.Int).Value = cursoUsuarioId;
                Obj_Comando.CommandText = MyQuery;
                adapter.SelectCommand = Obj_Comando;
                adapter.Fill(dt);
                adapter.Dispose();
            }

            return dt;
        }

        public DataTable Obtener_Puntos_Pausa_Preguntas(string ContenidoId, SqlConnection obj_Conexion)
        {
            SqlDataAdapter DataAdapter = new SqlDataAdapter();
            String MyQuery = "";
            DataTable dt = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter();
            Console.WriteLine(ContenidoId.ToString());
            using (SqlCommand Obj_Comando = obj_Conexion.CreateCommand())
            {
                MyQuery = " SELECT Minuto " +
                    "FROM Contenidos_Detalle " +
                    "WHERE Contenido_ID = @cid";
                Obj_Comando.Parameters.Add("@cid", SqlDbType.Int).Value = ContenidoId;
                Obj_Comando.CommandText = MyQuery;
                adapter.SelectCommand = Obj_Comando;
                adapter.Fill(dt);
                adapter.Dispose();
            }

            return dt;
        }

        public DataTable Obtener_Questionario(string contenido_Id, double minuto, SqlConnection obj_Conexion)
        {
            SqlDataAdapter DataAdapter = new SqlDataAdapter();
            String MyQuery = "";
            DataTable dt = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter();
            using (SqlCommand Obj_Comando = obj_Conexion.CreateCommand())
            {
                MyQuery = " SELECT ROW_NUMBER() OVER(ORDER BY Pregunta_Id DESC) AS Row, Pregunta_Id,Nombre_Pregunta " +
                    " ,'' AS Respuesta_Id " +
                    " FROM Preguntas " +
                    " WHERE Pregunta_ID IN ( " +
                    " SELECT Pregunta_ID " +
                    " FROM Cuestionario_Preguntas " +
                    "WHERE Cuestionario_ID = (" +
                    " SELECT Cuestionario_ID "+
                    "FROM Contenidos_Detalle " +
                    "WHERE Contenido_Detalle_ID = @cid AND Minuto = @min))";
                Obj_Comando.Parameters.Add("@cid", SqlDbType.Int).Value = contenido_Id;
                Obj_Comando.Parameters.Add("@min", SqlDbType.Decimal).Value = minuto;
                Obj_Comando.CommandText = MyQuery;
                adapter.SelectCommand = Obj_Comando;
                adapter.Fill(dt);
                adapter.Dispose();
            }
            return dt;
        }

        public DataTable Obtener_Respuestas(string contenido_Id, double minuto, SqlConnection obj_Conexion)
        {
            SqlDataAdapter DataAdapter = new SqlDataAdapter();
            String MyQuery = "";
            DataTable dt = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter();
            using (SqlCommand Obj_Comando = obj_Conexion.CreateCommand())
            {
                MyQuery = " SELECT pr.Pregunta_ID,pr.Respuesta_Id,r.Nombre_Respuesta,pr.Orden,pr.Correcta " +
                    " FROM Respuestas r " +
                    " JOIN Preguntas_Respuesta pr ON r.Respuesta_ID = pr.Respuesta_ID " +
                    " WHERE r.Respuesta_ID IN ( " +
                    " SELECT Respuesta_ID " +
                    " FROM Preguntas_Respuesta " +
                    "WHERE Pregunta_ID IN (" +
                    " SELECT Pregunta_ID " +
                    "FROM Cuestionario_Preguntas " +
                    "WHERE Cuestionario_ID = (" +
                    "SELECT Cuestionario_ID " +
                    "FROM Contenidos_Detalle" +
                    " WHERE Contenido_Detalle_ID = @cid AND Minuto = @min ) ) )";
                Obj_Comando.Parameters.Add("@cid", SqlDbType.Int).Value = contenido_Id;
                Obj_Comando.Parameters.Add("@min", SqlDbType.Decimal).Value = minuto;
                Obj_Comando.CommandText = MyQuery;
                adapter.SelectCommand = Obj_Comando;
                adapter.Fill(dt);
                adapter.Dispose();
            }
            return dt;
        }

        public long Relacion_Curso_Cuestionario(string curso_Usuario_ID, string curso_Cuestionario_ID, SqlConnection obj_Conexion)
        {
            String MyQuery = "";
            long Relacion_Id = 0;
            using (SqlCommand Obj_Comando = obj_Conexion.CreateCommand())
            {
                MyQuery = " INSERT INTO Cuestionario_Respuestas VALUES(@Curso_Usuario_ID, @Cuestionario_ID) ;SET @no_recibo = SCOPE_IDENTITY();";
                Obj_Comando.CommandText = MyQuery;
                Obj_Comando.Parameters.Add("@Curso_Usuario_ID", SqlDbType.Int).Value = curso_Usuario_ID;
                Obj_Comando.Parameters.Add("@Cuestionario_ID", SqlDbType.Int).Value = curso_Cuestionario_ID;

                SqlParameter parametro_no_recibo = new SqlParameter("@no_recibo", SqlDbType.Int);
                parametro_no_recibo.Direction = ParameterDirection.Output;
                Obj_Comando.Parameters.Add(parametro_no_recibo);
                Obj_Comando.ExecuteNonQuery();
                Relacion_Id = Convert.ToInt64(parametro_no_recibo.Value);
            }
            return Relacion_Id;
        }
        public int Guardar_Respuestas(string curso_Cuestionario_ID, string Pregunta_Id, string Respuesta_Id, SqlConnection obj_Conexion)
        {
            String MyQuery = "";
            int Insertados = 0;
            using (SqlCommand Obj_Comando = obj_Conexion.CreateCommand())
            {
                MyQuery = " INSERT INTO Cuestionario_Respuestas_Detalles VALUES( @curso_u_c_id,@p_id,@r_id)";
                Obj_Comando.CommandText = MyQuery;
                Obj_Comando.Parameters.Add("@curso_u_c_id", SqlDbType.Int).Value = curso_Cuestionario_ID;
                Obj_Comando.Parameters.Add("@p_id", SqlDbType.Int).Value = Pregunta_Id;
                Obj_Comando.Parameters.Add("@r_id", SqlDbType.Int).Value = Respuesta_Id;
                Insertados = Obj_Comando.ExecuteNonQuery();
            }
            return Insertados;
        }

        public int Registrar_Georeferencia(string Curso_Usuario_Cuestionario_ID, string latitud, string longitud, SqlConnection obj_Conexion)
        {
            String MyQuery = "";
            int Insertados = 0;
            using (SqlCommand Obj_Comando = obj_Conexion.CreateCommand())
            {
                MyQuery = " INSERT INTO Curso_Geolocalizacion VALUES( @curso_u_c_id,@p_lat,@p_long,getdate())";
                Obj_Comando.CommandText = MyQuery;
                Obj_Comando.Parameters.Add("@curso_u_c_id", SqlDbType.Int).Value = Curso_Usuario_Cuestionario_ID;
                Obj_Comando.Parameters.Add("@p_lat", SqlDbType.VarChar).Value = latitud;
                Obj_Comando.Parameters.Add("@p_long", SqlDbType.VarChar).Value = longitud;
                Insertados = Obj_Comando.ExecuteNonQuery();
            }
            return Insertados;
        }

        public int Registrar_Foto_Captura(string Id, string nombre_Foto, SqlConnection obj_Conexion)
        {
            String MyQuery = "";
            int Insertados = 0;
            using (SqlCommand Obj_Comando = obj_Conexion.CreateCommand())
            {
                MyQuery = " INSERT INTO Curso_Usuario_Fotos VALUES( @curso_u_c_id,@nombre_foto,getdate())";
                Obj_Comando.CommandText = MyQuery;
                Obj_Comando.Parameters.Add("@curso_u_c_id", SqlDbType.Int).Value = Id;
                Obj_Comando.Parameters.Add("@nombre_foto", SqlDbType.VarChar).Value = nombre_Foto;
                Insertados = Obj_Comando.ExecuteNonQuery();
            }
            return Insertados;
        }

        public int Registrar_Reproduccion(string Id, string latitud, string longitud, string nombre_Foto, SqlConnection obj_Conexion)
        {
            String MyQuery = "";
            int Insertados = 0;
            using (SqlCommand Obj_Comando = obj_Conexion.CreateCommand())
            {
                MyQuery = " INSERT INTO Ope_Reproducciones VALUES( 'KSSN links',@p_lat,@p_long,getdate(),0,@curso_u_c_id,@nombre_foto)";
                Obj_Comando.CommandText = MyQuery;
                Obj_Comando.Parameters.Add("@curso_u_c_id", SqlDbType.Int).Value = Id;
                Obj_Comando.Parameters.Add("@nombre_foto", SqlDbType.VarChar).Value = "http://200.33.34.9/Ws_Contenidos/Images/"+Id+"/"+nombre_Foto;
                Obj_Comando.Parameters.Add("@p_lat", SqlDbType.VarChar).Value = latitud;
                Obj_Comando.Parameters.Add("@p_long", SqlDbType.VarChar).Value = longitud;
                Insertados = Obj_Comando.ExecuteNonQuery();
            }
            return Insertados;
        }
    }
}