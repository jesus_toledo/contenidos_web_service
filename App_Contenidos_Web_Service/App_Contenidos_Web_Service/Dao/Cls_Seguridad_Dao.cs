﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace App_Contenidos_Web_Service.Dao
{
    public class Cls_Seguridad_Dao : iSeguridad_Dao
    {
        public bool Acceso(string nss, SqlConnection obj_Conexion)
        {
            SqlDataAdapter DataAdapter = new SqlDataAdapter();
            String MyQuery = "";
            DataTable dtAux = new DataTable();
            Boolean respuesta = false;
            object objRegistros;
            int cuentaRegistros = 0;
            using (SqlCommand comando = obj_Conexion.CreateCommand())
            {

                MyQuery = " SELECT COUNT(*) FROM Usuarios WHERE NSS = @nss";
                comando.Parameters.Add("@nss", SqlDbType.VarChar, 20, "NSS").Value = nss;
                comando.CommandText = MyQuery;
                objRegistros = comando.ExecuteScalar();
                Int32.TryParse(objRegistros.ToString(), out cuentaRegistros);
                //Comprobar que existe al menos un registro y regresar la respuesta
                if (cuentaRegistros > 0)
                    respuesta = true;
            }

            return respuesta;
        }

        public DataTable Obtener_Datos_Usuario(string ssn, SqlConnection obj_Conexion)
        {
            SqlDataAdapter DataAdapter = new SqlDataAdapter();
            String MyQuery = "";
            object objRegistros;
            DataTable dt = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter();
            using (SqlCommand Obj_Comando = obj_Conexion.CreateCommand())
            {
                MyQuery = " SELECT cu.Curso_Usuario_Id AS mCursoUsuarioId, u.Nombre +' '+ u.Apellido_Paterno +' '+ u.Apellido_Materno AS mNombre," +
                    "u.NSS AS mNss,cu.Motivo as mMotivo," +
                    "c.Nombre_Curso as mCurso,cu.Estatus as mEstatusCurso " +
                    "FROM Usuarios u,Curso_Usuario cu,Curso c WHERE u.NSS = @nss";
                MyQuery += " AND cu.Usuarios_ID = u.Usuarios_ID";
                MyQuery += " AND c.Curso_ID = cu.Curso_ID ORDER BY cu.Fecha_Creo asc";
                Obj_Comando.Parameters.Add("@nss", SqlDbType.VarChar, 20, "NSS").Value = ssn;
                Obj_Comando.CommandText = MyQuery;
                adapter.SelectCommand = Obj_Comando;
                adapter.Fill(dt);
                adapter.Dispose();
            }

            return dt;
        }

    }
}