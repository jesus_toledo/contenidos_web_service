﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_Contenidos_Web_Service.Dao
{
    interface iContenidos_Dao
    {
        DataTable Obtener_Contenidos(string cursoUsuarioId, SqlConnection obj_Conexion);
        DataTable Obtener_Puntos_Pausa_Preguntas(string ContenidoId, SqlConnection obj_Conexion);
        DataTable Obtener_Questionario(string contenido_Id, double minuto, SqlConnection obj_Conexion);
        DataTable Obtener_Respuestas(string contenido_Id, double minuto, SqlConnection obj_Conexion);
        Int64 Relacion_Curso_Cuestionario(string curso_Usuario_ID, string curso_Cuestionario_ID, SqlConnection obj_Conexion);
        int Guardar_Respuestas(string curso_Cuestionario_ID,string Pregunta_Id, string Respuesta_Id, SqlConnection obj_Conexion);
        int Registrar_Georeferencia(string Curso_Usuario_Cuestionario_ID, string latitud, string longitud, SqlConnection obj_Conexion);
        int Registrar_Foto_Captura(string iD, string nombre_Foto, SqlConnection obj_Conexion);
        int Registrar_Reproduccion(string id, string latitud, string longitud, string nombre_Foto, SqlConnection obj_Conexion);
    }
}
