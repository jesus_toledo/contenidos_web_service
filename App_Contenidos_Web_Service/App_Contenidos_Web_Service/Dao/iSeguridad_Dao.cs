﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_Contenidos_Web_Service.Dao
{
    public interface iSeguridad_Dao
    {
        DataTable Obtener_Datos_Usuario(string ssn, SqlConnection obj_Conexion);
        Boolean Acceso(string ssn, SqlConnection obj_Conexion);
    }
}
