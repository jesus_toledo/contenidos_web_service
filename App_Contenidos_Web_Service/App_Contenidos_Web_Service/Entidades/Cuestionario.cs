﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App_Contenidos_Web_Service.Entidades
{
    public class Cuestionario
    {
        public String pregunta_id { get; set; }
        public String respuesta_id { get; set; }
        public String nombre_pregunta { get; set; }
    }
}