﻿using App_Contenidos_Web_Service.Ayudante;
using App_Contenidos_Web_Service.Dao;
using App_Contenidos_Web_Service.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace App_Contenidos_Web_Service.Servicio
{
    public class Cls_Contenidos : iContenidos
    {
        private SqlConnection Obj_Conexion = null;
        private iContenidos_Dao Obj_Contenidos_Dao;

        public string Guardar_Respuestas(String Curso_Usuario_ID, String Curso_Cuestionario_ID, List<Cuestionario> mCuestionario)
        {
            int Registros = 0;
            Int64 Relacion_Id;
            Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Contenidos_Dao = new Cls_Contenidos_Dao();
            Obj_Conexion.Open();

            Relacion_Id = Obj_Contenidos_Dao.Relacion_Curso_Cuestionario(Curso_Usuario_ID, Curso_Cuestionario_ID, Obj_Conexion);

            foreach (Cuestionario bean in mCuestionario)
            {
                Obj_Contenidos_Dao.Guardar_Respuestas(Relacion_Id.ToString(), bean.pregunta_id,bean.respuesta_id, Obj_Conexion);
            }
            Obj_Conexion.Close();
            return Relacion_Id.ToString();
        }

        public DataTable Obtener_Contenidos(string cursoUsuarioId)
        {
            DataTable Dt;
            Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Contenidos_Dao = new Cls_Contenidos_Dao();
            Obj_Conexion.Open();
            Dt = Obj_Contenidos_Dao.Obtener_Contenidos(cursoUsuarioId, Obj_Conexion);
            Obj_Conexion.Close();
            return Dt;
        }

        public DataTable Obtener_Puntos_Pausa_Preguntas(string contenido_id)
        {
            DataTable Dt;
            Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Contenidos_Dao = new Cls_Contenidos_Dao();
            Obj_Conexion.Open();
            Dt = Obj_Contenidos_Dao.Obtener_Puntos_Pausa_Preguntas(contenido_id, Obj_Conexion);
            Obj_Conexion.Close();
            return Dt;
        }

        public DataTable Obtener_Questionario(string contenido_Id, double minuto)
        {
            DataTable Dt;
            Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Contenidos_Dao = new Cls_Contenidos_Dao();
            Obj_Conexion.Open();
            Dt = Obj_Contenidos_Dao.Obtener_Questionario(contenido_Id, minuto, Obj_Conexion);
            Obj_Conexion.Close();
            return Dt;
        }

        public DataTable Obtener_Respuestas(string contenido_Id, double minuto)
        {
            DataTable Dt;
            Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Contenidos_Dao = new Cls_Contenidos_Dao();
            Obj_Conexion.Open();
            Dt = Obj_Contenidos_Dao.Obtener_Respuestas(contenido_Id, minuto, Obj_Conexion);
            Obj_Conexion.Close();
            return Dt;
        }

        public string Registrar_Foto_Captura(string iD, string nombre_Foto)
        {
            int Registros = 0;
            int Relacion_Id;
            Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Contenidos_Dao = new Cls_Contenidos_Dao();
            Obj_Conexion.Open();

            Registros = Obj_Contenidos_Dao.Registrar_Foto_Captura(iD, nombre_Foto, Obj_Conexion);

            Obj_Conexion.Close();
            return Registros.ToString();
        }

        public string Registrar_Georeferencia(string Curso_Usuario_Cuestionario_ID, string latitud, string longitud)
        {
            int Registros = 0;
            int Relacion_Id;
            Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Contenidos_Dao = new Cls_Contenidos_Dao();
            Obj_Conexion.Open();

            Relacion_Id = Obj_Contenidos_Dao.Registrar_Georeferencia(Curso_Usuario_Cuestionario_ID, latitud, longitud, Obj_Conexion);
            
            Obj_Conexion.Close();
            return Relacion_Id.ToString();
        }

        public string Registrar_Reproduccion(string Id, string latitud, string longitud, string nombre_Foto)
        {
            int Registros = 0;
            int Relacion_Id;
            Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Contenidos_Dao = new Cls_Contenidos_Dao();
            Obj_Conexion.Open();

            Registros = Obj_Contenidos_Dao.Registrar_Reproduccion(Id, latitud, longitud, nombre_Foto, Obj_Conexion);

            Obj_Conexion.Close();
            return Registros.ToString();
        }
    }
}