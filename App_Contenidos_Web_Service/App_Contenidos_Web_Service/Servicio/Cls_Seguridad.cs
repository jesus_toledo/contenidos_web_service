﻿using App_Contenidos_Web_Service.Ayudante;
using App_Contenidos_Web_Service.Dao;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace App_Contenidos_Web_Service.Servicio
{
    public class Cls_Seguridad : iSeguridad
    {
        private SqlConnection Obj_Conexion = null;
        private iSeguridad_Dao Obj_Seguridad_Dao;
        public bool Login(string ssn)
        {
            Boolean Acceso = false;
            Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Seguridad_Dao = new Cls_Seguridad_Dao();
            Obj_Conexion.Open();
            Acceso = Obj_Seguridad_Dao.Acceso(ssn, Obj_Conexion);
            Obj_Conexion.Close();
            return Acceso;
        }

        public DataTable Obtener_Datos_Usuario(string ssn)
        {
            DataTable Dt;
            Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
            Obj_Seguridad_Dao = new Cls_Seguridad_Dao();
            Obj_Conexion.Open();
            Dt = Obj_Seguridad_Dao.Obtener_Datos_Usuario(ssn, Obj_Conexion);
            Obj_Conexion.Close();
            return Dt;
        }

    }
}