﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App_Contenidos_Web_Service.Entidades;

namespace App_Contenidos_Web_Service.Servicio
{
    interface iContenidos
    {
        DataTable Obtener_Puntos_Pausa_Preguntas(string contenido_id);
        DataTable Obtener_Contenidos(string cursoUsuarioId);
        DataTable Obtener_Questionario(string contenido_Id, double minuto);
        DataTable Obtener_Respuestas(string contenido_Id, double minuto);
        String Guardar_Respuestas(string Curso_Usuario_ID, string Cuestionario_ID, List<Cuestionario> mCuestionario);
        string Registrar_Georeferencia(string Curso_Usuario_Cuestionario_ID, string latitud, string longitud);
        string Registrar_Foto_Captura(string iD, string nombre_Foto);
        string Registrar_Reproduccion(string iD, string latitud, string longitud, string nombre_Foto);
    }
}
