﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_Contenidos_Web_Service.Servicio
{
    public interface iSeguridad
    {
        Boolean Login(string ssn);
        DataTable Obtener_Datos_Usuario(string ssn);
    }
}
