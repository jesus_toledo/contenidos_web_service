﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.IO;
using Newtonsoft.Json;

namespace Utils.Ayudante_JQuery
{
    public class Ayudante_JQuery
    {


        public static string onDataGrid(DataTable dt, int page, int rows)
        {

            string dato;
            page = (page == 0) ? 1 : page;
            rows = (rows == 0) ? 10 : rows;
            int start = (page - 1) * rows;
            int end = page * rows;
            end = (end > dt.Rows.Count) ? dt.Rows.Count : end;
            StringBuilder jsonBuilder = new StringBuilder();
            jsonBuilder.Append("{\"total\":" + dt.Rows.Count + ",\"rows\":[");
            for (int i = start; i < end; i++)
            {
                jsonBuilder.Append("{");
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    jsonBuilder.Append("\"");
                    jsonBuilder.Append(dt.Columns[j].ColumnName);
                    jsonBuilder.Append("\":\"");
                    dato = dt.Rows[i][j].ToString().Trim();
                    dato = dato.Replace("\r\n", " ").Replace("\n", " ").Replace("\r", " ").Replace(Environment.NewLine, " ");

                    jsonBuilder.Append(dato);

                    jsonBuilder.Append("\",");
                }
                jsonBuilder.Remove(jsonBuilder.Length - 1, 1);
                jsonBuilder.Append("},");
            }
            jsonBuilder.Remove(jsonBuilder.Length - 1, 1);
            jsonBuilder.Append("]}");
            return jsonBuilder.ToString();


        }
        public static string onComboBox(DataTable dt)
        {

            string dato;
            StringBuilder jsonBuilder = new StringBuilder();
            jsonBuilder.Append("[");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                jsonBuilder.Append("{");
                jsonBuilder.Append("\"");
                jsonBuilder.Append(dt.Columns[0].ColumnName);
                jsonBuilder.Append("\":\"");
                dato = dt.Rows[i][0].ToString().Trim();
                dato = dato.Replace("\r\n", " ").Replace("\n", " ").Replace("\r", " ").Replace(Environment.NewLine, " ");
                jsonBuilder.Append(dato);
                jsonBuilder.Append("\",");

                jsonBuilder.Append("\"");
                jsonBuilder.Append(dt.Columns[1].ColumnName);
                jsonBuilder.Append("\":\"");
                dato = dt.Rows[i][1].ToString().Trim();
                dato = dato.Replace("\r\n", " ").Replace("\n", " ").Replace("\r", " ").Replace(Environment.NewLine, " ");
                jsonBuilder.Append(dato);
                jsonBuilder.Append("\",");

                jsonBuilder.Remove(jsonBuilder.Length - 1, 1);
                jsonBuilder.Append("},");
            }
            jsonBuilder.Remove(jsonBuilder.Length - 1, 1);
            jsonBuilder.Append("]");
            return jsonBuilder.ToString();


        }

        public static string agregarTotalesGridEasyUI(string contenido, string totales)
        {

            string datos = "{[]}";
            string datos_aux;

            datos_aux = contenido.Substring(0, contenido.Length - 1);
            datos = datos_aux + ",\"footer\":" + "[" + totales + "]}";

            return datos;

        }


        public static string arregloToJSONF(string cadena_nombre, string cadena_valores)
        {
            string respuesta = "[]";
            char[] delimitador = { ';' };
            string[] nombres;
            string[] valores;
            int i, j;
            string s = "";
            int caracteres;

            StringBuilder jsonStringBuilder = new StringBuilder();
            StringWriter jsonStringWriter = new StringWriter(jsonStringBuilder);
            JsonWriter jsonWriter;

            jsonWriter = new JsonTextWriter(jsonStringWriter);

            nombres = cadena_nombre.Split(delimitador);
            valores = cadena_valores.Split(delimitador);

            if (nombres.Length > 0)
            {

                jsonWriter.Formatting = Formatting.None;
                jsonWriter.WriteStartArray();
                jsonWriter.WriteStartObject();

                for (i = 0; i < nombres.Length; i++)
                {
                    jsonWriter.WritePropertyName(nombres[i]);
                    jsonWriter.WriteValue(valores[i]);
                }

                jsonWriter.WriteEndObject();
            }

            jsonWriter.WriteEndArray();
            respuesta = jsonStringBuilder.ToString();

            if (respuesta.Length > 0)
            {
                caracteres = respuesta.Length;
                s = respuesta.Substring(1, caracteres - 2);
            }


            return s;
        }


        public static String Crear_Tabla_Formato_JSON(DataTable Dt_Datos)
        {
            StringBuilder Buffer = new StringBuilder();
            StringWriter Escritor = new StringWriter(Buffer);
            JsonWriter Escribir_Formato_JSON = new JsonTextWriter(Escritor);
            String Cadena_Resultado = String.Empty;

            try
            {
                Escribir_Formato_JSON.Formatting = Formatting.None;
                Escribir_Formato_JSON.WriteStartObject();
                Escribir_Formato_JSON.WritePropertyName("TOTAL");
                Escribir_Formato_JSON.WriteValue(Dt_Datos.Rows.Count.ToString());
                Escribir_Formato_JSON.WritePropertyName(Dt_Datos.TableName);

                if (Dt_Datos is DataTable)
                {
                    if (Dt_Datos.Rows.Count > 0)
                    {
                        Escribir_Formato_JSON.WriteStartArray();
                        foreach (DataRow FILA in Dt_Datos.Rows)
                        {
                            Escribir_Formato_JSON.WriteStartObject();
                            foreach (DataColumn COLUMNA in Dt_Datos.Columns)
                            {
                                if (!String.IsNullOrEmpty(FILA[COLUMNA.ColumnName].ToString()))
                                {
                                    Escribir_Formato_JSON.WritePropertyName(COLUMNA.ColumnName);
                                    Escribir_Formato_JSON.WriteValue(FILA[COLUMNA.ColumnName].ToString());
                                }
                            }
                            Escribir_Formato_JSON.WriteEndObject();
                        }

                        Escribir_Formato_JSON.WriteEndArray();
                        Escribir_Formato_JSON.WriteEndObject();
                        Cadena_Resultado = Buffer.ToString();
                    }
                    else Cadena_Resultado = "[]";
                }
                else Cadena_Resultado = "[]";
            }
            catch (Exception)
            {
                throw new Exception("Error al crear la cadena json para cargar un combo.");
            }
            return Cadena_Resultado;
        }
        public static string dataTableToJSONsintotalrenglones(DataTable dt)
        {
            StringBuilder jsonStringBuilder = new StringBuilder();
            StringWriter jsonStringWriter = new StringWriter(jsonStringBuilder);
            String cadenaFinal, valor, tipo;
            JsonWriter jsonWriter;
            int i, j;

            jsonWriter = new JsonTextWriter(jsonStringWriter);

            if (dt != null && dt.Rows.Count > 0)
            {
                jsonWriter.Formatting = Formatting.None;
                jsonWriter.WriteStartArray();

                for (i = 0; i < dt.Rows.Count; i++)
                {
                    jsonWriter.WriteStartObject();
                    for (j = 0; j < dt.Columns.Count; j++)
                    {
                        jsonWriter.WritePropertyName(dt.Columns[j].ColumnName.ToString().ToLower());
                        valor = dt.Rows[i][j].ToString();
                        jsonWriter.WriteValue(valor.Trim());
                    }
                    jsonWriter.WriteEndObject();

                }
                jsonWriter.WriteEndArray();
                cadenaFinal = jsonStringBuilder.ToString();
                return cadenaFinal;
            }

            return "{}";

        }
        public static string dataTableToObjectJSON(DataTable dt)
        {
            StringBuilder jsonStringBuilder = new StringBuilder();
            StringWriter jsonStringWriter = new StringWriter(jsonStringBuilder);
            String cadenaFinal, valor, tipo;
            JsonWriter jsonWriter;
            int i, j;

            jsonWriter = new JsonTextWriter(jsonStringWriter);

            if (dt != null && dt.Rows.Count > 0)
            {
                jsonWriter.Formatting = Formatting.None;

                for (i = 0; i < dt.Rows.Count; i++)
                {
                    jsonWriter.WriteStartObject();
                    for (j = 0; j < dt.Columns.Count; j++)
                    {
                        jsonWriter.WritePropertyName(dt.Columns[j].ColumnName.ToString().ToLower());
                        valor = dt.Rows[i][j].ToString();
                        jsonWriter.WriteValue(valor.Trim());
                    }
                    jsonWriter.WriteEndObject();
                    break;
                }
                cadenaFinal = jsonStringBuilder.ToString();
                return cadenaFinal;
            }

            return "{}";

        }
        public static string dataTableToJSON(DataTable dt)
        {
            StringBuilder jsonStringBuilder = new StringBuilder();
            StringWriter jsonStringWriter = new StringWriter(jsonStringBuilder);
            String cadenaFinal, valor, tipo;
            JsonWriter jsonWriter;
            int i, j;

            jsonWriter = new JsonTextWriter(jsonStringWriter);

            if (dt != null && dt.Rows.Count > 0)
            {
                jsonWriter.Formatting = Formatting.None;
                jsonWriter.WriteStartArray();

                for (i = 0; i < dt.Rows.Count; i++)
                {
                    jsonWriter.WriteStartObject();
                    for (j = 0; j < dt.Columns.Count; j++)
                    {
                        jsonWriter.WritePropertyName(dt.Columns[j].ColumnName.ToString().ToLower());
                        valor = dt.Rows[i][j].ToString();
                        jsonWriter.WriteValue(valor.Trim());
                    }
                    jsonWriter.WriteEndObject();

                }
                jsonWriter.WriteEndArray();
                cadenaFinal = "{\"total\":" + dt.Rows.Count + ",\"rows\":" + jsonStringBuilder.ToString() + "}";
                return cadenaFinal;
            }

            return "{\"total\":0,\"rows\":[]}";

        }
        public static String dataTableToJSON_TreeGrid(DataTable Dt_Padres, DataTable Dt_Hijos, string clave)
        {
            StringBuilder jsonStringBuilder = new StringBuilder();
            StringWriter jsonStringWriter = new StringWriter(jsonStringBuilder);
            String cadenaFinal, valor, tipo, hijos;
            int i, j;
            int k, l;
            DataTable Dt_Hijos_Seleccionados = new DataTable();

            if (Dt_Padres != null && Dt_Padres.Rows.Count > 0)
            {
                using (JsonWriter jsonWriter = new JsonTextWriter(jsonStringWriter))
                {
                    jsonWriter.Formatting = Formatting.None;
                    jsonWriter.WriteStartArray();
                    for (i = 0; i < Dt_Padres.Rows.Count; i++)
                    {

                        jsonWriter.WriteStartObject();
                        for (j = 0; j < Dt_Padres.Columns.Count; j++)
                        {
                            jsonWriter.WritePropertyName(Dt_Padres.Columns[j].ColumnName.ToString().ToLower());
                            valor = Dt_Padres.Rows[i][j].ToString();
                            jsonWriter.WriteValue(valor.Trim());
                        }
                        jsonWriter.WritePropertyName("state");
                        jsonWriter.WriteValue("closed");
                        jsonWriter.WritePropertyName("children");
                        jsonWriter.Formatting = Formatting.None;
                        jsonWriter.WriteStartArray();
                        //Dt_Hijos_Seleccionados = Dt_Hijos.Select(clave + " = '" + Dt_Padres.Rows[i][clave].ToString() + "'").CopyToDataTable();

                        DataRow[] Dr_Hijos = Dt_Hijos.Select(clave + " = '" + Dt_Padres.Rows[i][clave].ToString() + "'");
                        if (Dr_Hijos.Count() > 0)
                        {
                            Dt_Hijos_Seleccionados = Dr_Hijos.CopyToDataTable();
                        }
                        else
                        {
                            Dt_Hijos_Seleccionados.Rows.Clear();
                        }

                        for (k = 0; k < Dt_Hijos_Seleccionados.Rows.Count; k++)
                        {
                            jsonWriter.WriteStartObject();
                            for (l = 0; l < Dt_Hijos_Seleccionados.Columns.Count; l++)
                            {
                                jsonWriter.WritePropertyName(Dt_Hijos_Seleccionados.Columns[l].ColumnName.ToString().ToLower());
                                valor = Dt_Hijos_Seleccionados.Rows[k][l].ToString();
                                jsonWriter.WriteValue(valor.Trim());
                            }
                            jsonWriter.WriteEndObject();
                        }
                        jsonWriter.WriteEndObject();
                    }

                }
                cadenaFinal = "{\"total\":" + Dt_Padres.Rows.Count + ",\"rows\":" + jsonStringBuilder.ToString() + "}";
                return cadenaFinal;
            }
            else
                return "{\"total\":0,\"rows\":[]}";
        }
        //public static DataTable Obtener_Dt_Ingresos(String Ingresos)
        //{
        //    DataTable P_Dt_Cambios_Categoria = new DataTable(typeof(Cls_Parametros_Cambio_Categoria).Name);

        //    //if (P_List_Cambio_Categoria != null)
        //    {
        //        //if (P_List_Cambio_Categoria.Count > 0)
        //        {
        //            //Obtiene todas las propiedades de la clase
        //            PropertyInfo[] Arr_Propiedades_Cambios_Categoria = typeof(Cls_Parametros_Cambio_Categoria).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        //            foreach (PropertyInfo Propiedades_Cambios_Categoria in Arr_Propiedades_Cambios_Categoria)
        //            {
        //                //Agrega las columnas con los nombres de las propiedades de la clase
        //                P_Dt_Cambios_Categoria.Columns.Add(Propiedades_Cambios_Categoria.Name);
        //            }
        //            foreach (Cls_Parametros_Cambio_Categoria Cambio_Categoria in P_List_Cambio_Categoria)
        //            {
        //                var Valores_Cambios_Categoria = new Object[Arr_Propiedades_Cambios_Categoria.Length];
        //                for (int Cont_Propiedades = 0; Cont_Propiedades < Arr_Propiedades_Cambios_Categoria.Length; Cont_Propiedades++)
        //                {
        //                    //Inserta los valores de las propiedades de la clase
        //                    Valores_Cambios_Categoria[Cont_Propiedades] = Arr_Propiedades_Cambios_Categoria[Cont_Propiedades].GetValue(Cambio_Categoria, null);
        //                }
        //                P_Dt_Cambios_Categoria.Rows.Add(Valores_Cambios_Categoria);
        //            }
        //        }
        //    }

        //    return P_Dt_Cambios_Categoria;
        //}
    }
}
