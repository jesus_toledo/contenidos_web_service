﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App_Contenidos_Web_Service.Util
{
    #region Variables Publicas
    public class Cls_Mensajes_Servidor
    {
        public string mensaje { get; set; }
        public string datos_auxiliares { get; set; }
        public string dato1 { get; set; }
        public string dato2 { get; set; }
        public string dato3 { get; set; }
        public string dato4 { get; set; }
        public string dato5 { get; set; }
        public string dato6 { get; set; }
        public string dato7 { get; set; }

    }
    #endregion

}