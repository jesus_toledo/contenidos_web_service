﻿using App_Contenidos_Web_Service.Servicio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using Newtonsoft.Json;
using System.IO;
using System.Data;
using Utils.Ayudante_JQuery;
using App_Contenidos_Web_Service.Entidades;
using App_Contenidos_Web_Service.Util;
using System.Drawing;
using System.Web.Hosting;

namespace App_Contenidos_Web_Service
{
    /// <summary>
    /// Descripción breve de Ws_Contenidos
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class Ws_Contenidos : System.Web.Services.WebService
    {
        private iSeguridad Obj_Seguridad;
        private iContenidos Obj_Contenidos;
        private const Int32 Constante_Milisegundos = 1000;
        private const Int32 Constante_Segundos = 60;
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public void Acceso(String Nss)
        {
            Obj_Seguridad = new Cls_Seguridad();
            String Cadena_Resultado = String.Empty;
            StringBuilder Buffer = new StringBuilder();
            StringWriter Escritor = new StringWriter(Buffer);
            JsonWriter Escribir_Formato_JSON = new JsonTextWriter(Escritor);
            Escribir_Formato_JSON.Formatting = Formatting.None;
            //Escribir_Formato_JSON.WriteStartArray();
            Escribir_Formato_JSON.WriteStartObject();
            Escribir_Formato_JSON.WritePropertyName("result");
            Escribir_Formato_JSON.WriteValue(Obj_Seguridad.Login(Nss).ToString());
            Escribir_Formato_JSON.WriteEndObject();
            //Escribir_Formato_JSON.WriteEndArray();
            Cadena_Resultado = Buffer.ToString();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(Cadena_Resultado);
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public void Obtener_Datos_Usuario(String Nss)
        {
            String Cadena_Resultado = "{}";
            DataTable Dt_Datos = new DataTable();
            Obj_Seguridad = new Cls_Seguridad();
            Dt_Datos = Obj_Seguridad.Obtener_Datos_Usuario(Nss);
            Cadena_Resultado = Ayudante_JQuery.dataTableToObjectJSON(Dt_Datos);
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(Cadena_Resultado);
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public void Obtener_Contenidos(String CursoUsuarioId)
        {
            String Cadena_Resultado = "{}";
            DataTable Dt_Datos = new DataTable();
            Obj_Contenidos = new Cls_Contenidos();
            Dt_Datos = Obj_Contenidos.Obtener_Contenidos(CursoUsuarioId);
            Cadena_Resultado = Ayudante_JQuery.dataTableToJSONsintotalrenglones(Dt_Datos);
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(Cadena_Resultado);
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public void Obtener_Puntos_Pausa_Preguntas(String contenido_id)
        {
            String Cadena_Resultado = "{}";
            StringBuilder sbResultado = new StringBuilder();
            String[] ArrayResulado;
            Double intMinuto = 0;
            Double intSegundo = 0;
            int intMilisegundo = 0;
            DataTable Dt_Datos = new DataTable();
            Obj_Contenidos = new Cls_Contenidos();
            Dt_Datos = Obj_Contenidos.Obtener_Puntos_Pausa_Preguntas(contenido_id);
            //Cadena_Resultado = Ayudante_JQuery.dataTableToJSONsintotalrenglones(Dt_Datos);
            //ArrayResulado = Dt_Datos.Rows.OfType<DataRow>().Select(k => k[0].ToString()).ToArray();
            foreach (DataRow r in Dt_Datos.Rows)
            {
                Double.TryParse(r["minuto"].ToString(), out intMinuto);
                intSegundo = intMinuto * Constante_Segundos;
                intMilisegundo = Convert.ToInt32( intSegundo * Constante_Milisegundos );
                sbResultado.Append(intMilisegundo.ToString());
                sbResultado.Append(",");
            }
            sbResultado.Remove(sbResultado.Length-1, 1);
            Context.Response.Clear();
            Context.Response.ContentType = "text/plain";
            Context.Response.Write(sbResultado.ToString());
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public void Obtener_Questionario(String Contenido_Id, String Milisegundo)
        {
            Double intMinuto = 0;
            Double intSegundo = 0;
            int intMilisegundo = 0;
            String Cadena_Resultado = "{}";
            DataTable Dt_Datos = new DataTable();
            Obj_Contenidos = new Cls_Contenidos();

            Int32.TryParse(Milisegundo, out intMilisegundo);
            intSegundo = intMilisegundo / 1000;
            intMinuto = intSegundo / 60;            

            Dt_Datos = Obj_Contenidos.Obtener_Questionario(Contenido_Id, intMinuto);
            Cadena_Resultado = Ayudante_JQuery.dataTableToJSONsintotalrenglones(Dt_Datos);
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(Cadena_Resultado);
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public void Obtener_Respuestas(String Contenido_Id, String Milisegundo)
        {
            Double intMinuto = 0;
            Double intSegundo = 0;
            int intMilisegundo = 0;
            String Cadena_Resultado = "{}";
            DataTable Dt_Datos = new DataTable();
            Obj_Contenidos = new Cls_Contenidos();

            Int32.TryParse(Milisegundo, out intMilisegundo);
            intSegundo = intMilisegundo / 1000;
            intMinuto = intSegundo / 60;

            Dt_Datos = Obj_Contenidos.Obtener_Respuestas(Contenido_Id, intMinuto);
            Cadena_Resultado = Ayudante_JQuery.dataTableToJSONsintotalrenglones(Dt_Datos);
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(Cadena_Resultado);
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public void Recibir_Respuestas(String Curso_Usuario_ID, String Cuestionario_ID,String Obj_Respuestas)
        {
            String Actualizados = String.Empty;
            DataTable Dt_Datos = new DataTable();
            List<Cuestionario> mCuestionario = new List<Cuestionario>();
            Obj_Contenidos = new Cls_Contenidos();

            Cls_Mensajes_Servidor Obj_Mensaje_Servidor = new Cls_Mensajes_Servidor();
            JsonSerializerSettings configuracionJson = new JsonSerializerSettings();
            configuracionJson.NullValueHandling = NullValueHandling.Ignore;

            try
            {
                mCuestionario = JsonConvert.DeserializeObject<List<Cuestionario>>(Obj_Respuestas);
                Actualizados = Obj_Contenidos.Guardar_Respuestas(Curso_Usuario_ID, Cuestionario_ID,mCuestionario);
            }
        catch (Exception ex)
        {
            Obj_Mensaje_Servidor.mensaje = ex.Message;
        }
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(Actualizados);
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public void Registrar_Geolocalizacion(String ID, String Latitud, String Longitud)
        {
            String Actualizados = String.Empty;
            DataTable Dt_Datos = new DataTable();
            List<Cuestionario> mCuestionario = new List<Cuestionario>();
            Obj_Contenidos = new Cls_Contenidos();

            Cls_Mensajes_Servidor Obj_Mensaje_Servidor = new Cls_Mensajes_Servidor();
            JsonSerializerSettings configuracionJson = new JsonSerializerSettings();
            configuracionJson.NullValueHandling = NullValueHandling.Ignore;

            try
            {
                Actualizados = Obj_Contenidos.Registrar_Georeferencia(ID, Latitud, Longitud);
            }
            catch (Exception ex)
            {
                Obj_Mensaje_Servidor.mensaje = ex.Message;
            }
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(Actualizados);
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
        public void Registrar_Reproduccion(String ID, String Latitud, String Longitud, String base64String)
        {
            String Actualizados = String.Empty;
            DataTable Dt_Datos = new DataTable();
            List<Cuestionario> mCuestionario = new List<Cuestionario>();
            Obj_Contenidos = new Cls_Contenidos();
            String Nombre_Foto = String.Empty;
            Cls_Mensajes_Servidor Obj_Mensaje_Servidor = new Cls_Mensajes_Servidor();
            JsonSerializerSettings configuracionJson = new JsonSerializerSettings();
            configuracionJson.NullValueHandling = NullValueHandling.Ignore;

            try
            {
                Nombre_Foto = "Pic_" + ID + "_" + DateTime.Now.Ticks.ToString() + ".jpg"; 
                Guardar_Foto(base64String,ID,Nombre_Foto);
                Actualizados = Obj_Contenidos.Registrar_Reproduccion(ID,Latitud,Longitud, Nombre_Foto);

            }
            catch (Exception ex)
            {
                Obj_Mensaje_Servidor.mensaje = ex.Message;
                String mensaje = Obj_Mensaje_Servidor.mensaje;
                Console.Write("el error al guardar la foto es: " + mensaje);
            }
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(Actualizados);
        }

        private void Guardar_Foto(String ImageText, String Curso_Id,String Nombre)
        {
            string Ruta;
            string resulFile;
            Image imagen = Base64ToImage(ImageText);
            Ruta = HostingEnvironment.MapPath("~/Images/"+Curso_Id+"/");
            resulFile = Path.Combine(Ruta, Nombre);

            if (!Directory.Exists(Path.GetDirectoryName(resulFile)))
                Directory.CreateDirectory(Path.GetDirectoryName(resulFile));
            if (File.Exists(resulFile))
            {
                File.Delete(resulFile);
            }
            imagen.Save(resulFile);
        }
        public Image Base64ToImage(string cadena)
        {
            byte[] imageBytes = Convert.FromBase64String(cadena);
            MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
            ms.Write(imageBytes, 0, imageBytes.Length);
            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
            return image;
        }
        public string FixBase64ForImage(string Image)
        {
            System.Text.StringBuilder sbText = new System.Text.StringBuilder(Image, Image.Length);
            sbText.Replace("\r\n", String.Empty); sbText.Replace(" ", String.Empty);
            return sbText.ToString();
        }
    }
}
